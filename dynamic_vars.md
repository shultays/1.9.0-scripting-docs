# Dynamic Variables


    Dynamic variables are read only variables that can be used in effects & triggers that accept variables.
    When they are used, the game will compute and return the value of the dynamic variable.
## Table of Content

* [global](#dynamic-variables-for-scope-global)
* [country](#dynamic-variables-for-scope-country)
* [state](#dynamic-variables-for-scope-state)
* [unit_leader](#dynamic-variables-for-scope-unit_leader)

## Dynamic variables for scope global

### countries
* description: (Trigger) get array of all countries (including non existing

### date
* description: (Trigger) get date value that can be comparable to other date values and localized using GetDateString/GetDateStringShortMonth/GetDateStringNoHour/GetDateStringNoHourLong scripted locs

### ideology_groups
* description: (Trigger) array of objects in ideology_groups database

### majors
* description: (Trigger) get array of all majors (including non existing

### num_days
* description: (Trigger) current total days

### operations
* description: (Trigger) array of objects in operations database

### year
* description: (Trigger) current year

### difficulty
* description: check if the difficulty is above or below specified value 0-2 (difficulty enum). Example: difficulty > 0 (above easy)

### threat
* description: check the global threat value. 0-1 value

## Dynamic variables for scope country

### ai_attitude_allied_weight
* description: (Trigger) weight for an ai attitude attitude_alliedagainst country. Example: GER.ai_attitude_allied_weight@ENG

### ai_attitude_friendly_weight
* description: (Trigger) weight for an ai attitude attitude_friendlyagainst country. Example: GER.ai_attitude_friendly_weight@ENG

### ai_attitude_hostile_weight
* description: (Trigger) weight for an ai attitude attitude_hostileagainst country. Example: GER.ai_attitude_hostile_weight@ENG

### ai_attitude_is_threatened
* description: (Trigger) returns 1 if ai is threatened

### ai_attitude_neutral_weight
* description: (Trigger) weight for an ai attitude attitude_neutralagainst country. Example: GER.ai_attitude_neutral_weight@ENG

### ai_attitude_outraged_weight
* description: (Trigger) weight for an ai attitude attitude_outragedagainst country. Example: GER.ai_attitude_outraged_weight@ENG

### ai_attitude_protective_weight
* description: (Trigger) weight for an ai attitude attitude_protectiveagainst country. Example: GER.ai_attitude_protective_weight@ENG

### ai_attitude_threatened_weight
* description: (Trigger) weight for an ai attitude attitude_threatenedagainst country. Example: GER.ai_attitude_threatened_weight@ENG

### ai_attitude_wants_ally
* description: (Trigger) returns 1 if ai wants ally

### ai_attitude_wants_antagonize
* description: (Trigger) returns 1 if ai wants antagonize

### ai_attitude_wants_ignore
* description: (Trigger) returns 1 if ai wants ignore

### ai_attitude_wants_protect
* description: (Trigger) returns 1 if ai wants protect

### ai_attitude_wants_weaken
* description: (Trigger) returns 1 if ai wants weaken

### ai_strategy_activate_crypto
* description: (Trigger) ai strategy value activate_crypto against country. Example: GER.ai_strategy_activate_crypto@ENG

### ai_strategy_alliance
* description: (Trigger) ai strategy value alliance against country. Example: GER.ai_strategy_alliance@ENG

### ai_strategy_antagonize
* description: (Trigger) ai strategy value antagonize against country. Example: GER.ai_strategy_antagonize@ENG

### ai_strategy_befriend
* description: (Trigger) ai strategy value befriend against country. Example: GER.ai_strategy_befriend@ENG

### ai_strategy_conquer
* description: (Trigger) ai strategy value conquer against country. Example: GER.ai_strategy_conquer@ENG

### ai_strategy_consider_weak
* description: (Trigger) ai strategy value consider_weak against country. Example: GER.ai_strategy_consider_weak@ENG

### ai_strategy_contain
* description: (Trigger) ai strategy value contain against country. Example: GER.ai_strategy_contain@ENG

### ai_strategy_declare_war
* description: (Trigger) ai strategy value declare_war against country. Example: GER.ai_strategy_declare_war@ENG

### ai_strategy_decrypt_target
* description: (Trigger) ai strategy value decrypt_target against country. Example: GER.ai_strategy_decrypt_target@ENG

### ai_strategy_dont_defend_ally_borders
* description: (Trigger) ai strategy value dont_defend_ally_borders against country. Example: GER.ai_strategy_dont_defend_ally_borders@ENG

### ai_strategy_force_defend_ally_borders
* description: (Trigger) ai strategy value force_defend_ally_borders against country. Example: GER.ai_strategy_force_defend_ally_borders@ENG

### ai_strategy_ignore
* description: (Trigger) ai strategy value ignore against country. Example: GER.ai_strategy_ignore@ENG

### ai_strategy_ignore_claim
* description: (Trigger) ai strategy value ignore_claim against country. Example: GER.ai_strategy_ignore_claim@ENG

### ai_strategy_influence
* description: (Trigger) ai strategy value influence against country. Example: GER.ai_strategy_influence@ENG

### ai_strategy_invade
* description: (Trigger) ai strategy value invade against country. Example: GER.ai_strategy_invade@ENG

### ai_strategy_occupation_policy
* description: (Trigger) ai strategy value occupation_policy against country. Example: GER.ai_strategy_occupation_policy@ENG

### ai_strategy_prepare_for_war
* description: (Trigger) ai strategy value prepare_for_war against country. Example: GER.ai_strategy_prepare_for_war@ENG

### ai_strategy_protect
* description: (Trigger) ai strategy value protect against country. Example: GER.ai_strategy_protect@ENG

### ai_strategy_send_volunteers_desire
* description: (Trigger) ai strategy value send_volunteers_desire against country. Example: GER.ai_strategy_send_volunteers_desire@ENG

### ai_strategy_support
* description: (Trigger) ai strategy value support against country. Example: GER.ai_strategy_support@ENG

### air_intel
* description: (Trigger) air intel against a target country. example GER.air_intel@ENG

### allies
* description: (Trigger) array of allies (faction members). prefer using faction_members instead

### army_intel
* description: (Trigger) army intel against a target country. example GER.army_intel@ENG

### army_leaders
* description: (Trigger) all army leaders of a country

### autonomy_ratio
* description: (Trigger) autonomy of scope country. -1 if not a subject

### capital
* description: (Trigger) capital state of the country

### civilian_intel
* description: (Trigger) civilian intel against a target country. example GER.civilian_intel@ENG

### command_power
* description: (Trigger) total command power of country

### controlled_states
* description: (Trigger) array of controlled states

### core_compliance
* description: (Trigger) returns core compliance of target country

### core_resistance
* description: (Trigger) returns core resistance of target country

### core_states
* description: (Trigger) array of core states

### cryptology_defense_level
* description: (Trigger) cryptology defense level of a country

### current_party_ideology_group
* description: (Trigger) returns the token for current party ideology group

### days_mission_timeout
* description: (Trigger) timeout in days for a specific timed mission, mission type token is defined in target. example: days_mission_timeout@GER_mefo_bills_mission

### decryption_speed
* description: (Trigger) total encryption strength of a country that is needed

### encryption_strength
* description: (Trigger) total encryption strength of a country that is needed

### enemies
* description: (Trigger) array of enemies at war with

### enemies_of_allies
* description: (Trigger) array of enemies of allies

### exiles
* description: (Trigger) exile host of this country

### faction_leader
* description: (Trigger) faction leader of this country's faction

### faction_members
* description: (Trigger) array of faction members

### fuel_k
* description: (Trigger) total fuel of country in thousands

### highest_party_ideology
* description: (Trigger) ideology of the most popular party. Can exclude the ruling party by using @exclude_ruling_party. Example: highest_party_ideology OR highest_party_ideology@exclude_ruling_party

### highest_party_popularity
* description: (Trigger) popularity size of the most popular party [0.00, 1.00]. Can exclude the ruling party by using @exclude_ruling_party. Example: highest_party_popularity OR highest_party_popularity@exclude_ruling_party

### host
* description: (Trigger) exile host of this country

### legitimacy
* description: (Trigger) legitimacy of scope country. -1 if not an exile

### manpower
* description: (Trigger) DEPRECATED, MAY OVERFLOW. total manpower of country

### manpower_k
* description: (Trigger) total manpower of country in thousands

### max_available_manpower
* description: (Trigger) DEPRECATED, MAY OVERFLOW. total available manpower of country

### max_available_manpower_k
* description: (Trigger) total available manpower of country in thousands

### max_fuel_k
* description: (Trigger) max fuel of country in thousands

### max_manpower
* description: (Trigger) DEPRECATED, MAY OVERFLOW. maximum manpower of country

### max_manpower_k
* description: (Trigger) maximum manpower of country in thousands

### modifier
* description: (Trigger) a modifier stored in country scope

### navy_intel
* description: (Trigger) navy intel against a target country. example GER.navy_intel@ENG

### navy_leaders
* description: (Trigger) all navy leaders of a country

### neighbors
* description: (Trigger) array of neighbors

### neighbors_owned
* description: (Trigger) array of neighbors to owned states

### num_armies
* description: (Trigger) number of armies

### num_armies_in_state
* description: (Trigger) number of armies in state, state is in target. example num_armies_in_state@123

### num_armies_with_type
* description: (Trigger) number of armies with dominant type, dominant type is defined in target. example: num_armies_with_type@light_armor

### num_battalions
* description: (Trigger) number of battalions

### num_battalions_with_type
* description: (Trigger) number of battalions with sub unit type, sub unit type is defined in target. example: num_battalions_with_type@light_armor

### num_controlled_states
* description: (Trigger) number of controlled states

### num_core_states
* description: (Trigger) number of core states

### num_deployed_planes
* description: (Trigger) number of deployed planes

### num_deployed_planes_with_type
* description: (Trigger) number of deployed planes with equipment type. example num_deployed_planes_with_type@fighter

### num_equipment
* description: (Trigger) number of equipment in country. example num_equipment@infantry_equipment

### num_equipment_in_armies
* description: (Trigger) number of equipment in armies of the country, equipment type token is defined in target. example num_equipment_in_armies@infantry_equipment

### num_equipment_in_armies_k
* description: (Trigger) number of equipment in armies of the country in thousands, equipment type token is defined in target. example num_equipment_in_armies_k@infantry_equipment

### num_owned_controlled_states
* description: (Trigger) number of owned and core states

### num_owned_states
* description: (Trigger) number of owned states

### num_ships
* description: (Trigger) number of ships

### num_ships_with_type
* description: (Trigger) number of ships controlled in country, ship type is defined in target. example num_ships_with_type@carrier. can be a sub unit def type or one of carrier,capital,screen, submarine

### num_target_equipment_in_armies
* description: (Trigger) number of equipment required in armies of the country, equipment type token is defined in target. example num_target_equipment_in_armies@infantry_equipment

### num_target_equipment_in_armies_k
* description: (Trigger) number of equipment required in armies of the country in thousands, equipment type token is defined in target. example num_target_equipment_in_armies_k@infantry_equipment

### occupied_countries
* description: (Trigger) array of occupied countries

### operatives
* description: (Trigger) all operatives of a country

### opinion
* description: (Trigger) opinion of a country targeted on another one. example GER.opinion@ENG

### original_tag
* description: (Trigger) returns the original tag of a country

### overlord
* description: (Trigger) master of this subject

### owned_controlled_states
* description: (Trigger) array owned and core states

### owned_states
* description: (Trigger) array of owned states

### party_popularity
* description: (Trigger) popularity of targeted party [0.00, 1.00]. example party_popularity@democratic. May also target ruling_party. This also supports country variables, so you can party_popularity@my_var_name for variables that store ideologies

### political_power
* description: (Trigger) total political power of country

### potential_and_current_enemies
* description: (Trigger) array of potential and actual enemies

### resource
* description: (Trigger) number of surplus resources in country, resource type is defined in target resource@steel

### resource_consumed
* description: (Trigger) number of resources consumed by country, resource type is defined in target resource_consumed@steel

### resource_exported
* description: (Trigger) number of resources exported by country, resource type is defined in target resource_exported@steel

### resource_imported
* description: (Trigger) number of resources imported by country, resource type is defined in target resource_imported@steel

### resource_produced
* description: (Trigger) number of resources produced by country, resource type is defined in target. example resource_produced@steel

### stability
* description: (Trigger) stability of a country

### subjects
* description: (Trigger) array of subjects

### agency_upgrade_number
* description: Checks the number of upgrade done in the intelligence agency. 
agency_upgrade_number > 4

### ai_irrationality
* description: check the ai irrationality value

### ai_wants_divisions
* description: Will compare towards the amount of divisions an ai wants to have.

### alliance_naval_strength_ratio
* description: Compares the estimated naval strength between the scope country, his allies and his enemies.

### alliance_strength_ratio
* description: Compares the estimated army strength between the scope country, his allies and his enemies.

### amount_manpower_in_deployment_queue
* description: Checks for amount manpower currently in deploymentview. amount_manpower_in_training > 10

### amount_research_slots
* description: check number of research current research slots 
 amount_research_slots > 2

### any_war_score
* description: compares the warscore of all wars in a country to see if any fullfills the comparison condition 0-100 - Example: any_war_score > 40

### casualties
* description: Check the amount of casualties a country has suffered in all of it's wars

### casualties_k
* description: Check the amount of casualties in thousands a country has suffered in all of it's wars

### command_power_daily
* description: Checks if daily command power increase is more or less that specified value 
 command_power_daily > 1.5

### compare_autonomy_progress_ratio
* description: check if autonomy progress ratio is higher than value, example:
compare_autonomy_progress_ratio > 0.5

### conscription_ratio
* description: Checks  conscription ratio of the country compared to target conscription ratio.


### convoy_threat
* description: A trigger to check convoy threat for a country. Controlled by NAVAL_CONVOY_DANGER defines. Returns a value between 0 and 1. Example convoy_threat > 0.5 

### current_conscription_amount
* description: Checks the current conscription amount of the country.


### days_since_capitulated
* description: Checks the number of days since the country last capitulated, even if it is no longer capitulated.
	If it has not ever capitulated, the value is extremely large.
	It is recommended to combine this with has_capitulated = yes when you specifically want to ignore non-active capitulations.
Examples:
	HOL = { has_capitulated = yes days_since_capitulated > 60 } # The Netherlands has been capitulated for more than two months
	FRA = { has_capitulated = yes days_since_capitulated < 21 } # France has capitulated sometime within the past three weeks
	GER = { OR = { has_capitulated = no days_since_capitulated > 14 } } # Germany is not both actively and recently capitulated


### decryption_progress
* description: checks decryption ratio against a country. Example: 
decryption_progress = { 
 target = GER
 value > 0.5
} 
#or decryption_progress@GER as variable


### enemies_naval_strength_ratio
* description: Compares the estimated navy strength between the scope country and all its enemies

### enemies_strength_ratio
* description: Compares the estimated army strength between the scope country and all its enemies

### foreign_manpower
* description: check the amount of foreign garrison manpower we have

### fuel_ratio
* description: Compares the fuel ratio to a variable.
Example: fuel_ratio > 0.5

### garrison_manpower_need
* description: check the amount of manpower needed by garrisons

### has_added_tension_amount
* description: Compare if the country has added above or below the specified ammount of tension

### has_collaboration
* description: checks the collaboration in a target country with our currently scoped country. Example: 
has_collaboration = { 
 target = GER
 value > 0.5
} 
#or has_collaboration@GER as variable


### has_legitimacy
* description: Check scope country legitimacy 0-100: Example has_legitimacy < 60

### has_manpower
* description: check amount of manpower

### has_political_power
* description: check amount of political power

### has_stability
* description: check value of stability 0-1: Example has_stability < 0.6

### has_war_support
* description: check value of war_support 0-1: Example has_war_support < 0.6

### land_doctrine_level
* description: checks researched land doctrine level

### manpower_per_military_factory
* description: Number of available manpower per factory the country has. Excluding dockyards.
manpower_per_military_factory < 1000

### mine_threat
* description: A trigger to check how dangerous enemy mines are for a country. Controlled by NAVAL_MINE_DANGER defines. Returns a value between 0 and 1. Example mine_threat > 0.5 

### network_national_coverage
* description: checks network national coverage you have over a country. Example: 
network_national_coverage = { 
 target = GER
 value > 0.5
} 


### num_divisions
* description: Will compare towards the amount of divisions a country has control over, if strength matters use has_army_size.

### num_faction_members
* description: Compares the number of members in the faction for the current country. 
 Example: num_faction_members > 10

### num_fake_intel_divisions
* description: Will compare towards the amount of fake intel divisions a country has control over. .

### num_free_operative_slots
* description: Checks the number of operative a country can recruit right now.
Note that this is not necessarily greater than zero if num_operative_slots returned a number greater than the number of operative.

### num_occupied_states
* description: check the number of states occupied by nation

### num_of_available_civilian_factories
* description: check amount of available civilian factories

### num_of_available_military_factories
* description: check amount of available military factories

### num_of_available_naval_factories
* description: check amount of available naval factories

### num_of_civilian_factories
* description: check amount of civilian factories

### num_of_civilian_factories_available_for_projects
* description: check amount of civilian factories available for a new project to use

### num_of_controlled_states
* description: check amount of controlled stats

### num_of_factories
* description: check amount of total factories

### num_of_military_factories
* description: check amount of military factories

### num_of_naval_factories
* description: check amount of naval factories

### num_of_nukes
* description: check amount of nukes

### num_of_operatives
* description: Checks the number of operatives the country controls

### num_operative_slots
* description: Checks the number of available operative slots a country has.
If this differs from the number of operative, this does not mean the country can recruit an operative, but that it will eventually be able to.

### num_subjects
* description: check the number of subjects of nation

### num_tech_sharing_groups
* description: checks how many groups a nation is a member of

### original_research_slots
* description: check number of research slots at start of game

### political_power_daily
* description: Checks if daily political power increase is more or less that specified value 
 political_power_daily > 1.5

### political_power_growth
* description: Check the value of political power daily growth.Exacmple: political_power_growth > 0

### surrender_progress
* description: check if a country is close to surrendering

### target_conscription_amount
* description: Checks the target conscription amount of the country.


## Dynamic variables for scope state

### arms_factory_level
* description: (Trigger) military factory level in the state

### building_level
* description: (Trigger) building level of a building with type, uses target as building type. example building_level@arms_factory

### controller
* description: (Trigger) controller of the state

### core_countries
* description: (Trigger) countries that cores the state

### damaged_building_level
* description: (Trigger) damaged building level of a building with type, uses target as building type. example damaged_building_level@arms_factory

### distance_to
* description: (Trigger) distance to another state, uses target as another state. example: 123.distance_to@124

### industrial_complex_level
* description: (Trigger) civilian factor level in the state

### infrastructure_level
* description: (Trigger) infrastructure level in the state

### modifier
* description: (Trigger) value of modifier stored in this state, uses target as modifier token, example: 123.modifier@local_manpower

### non_damaged_building_level
* description: (Trigger) non damaged building level of a building with type, uses target as building type. example non_damaged_building_level@arms_factory

### owner
* description: (Trigger) owner of the state

### resource
* description: (Trigger) resources produced in state. example resource@steel

### compliance
* description: Compares the current compliance level of a state to a value. Example: compliance > 50 

### compliance_speed
* description: Compares the current compliance speed of a state to a value. Example: compliance_speed > 50 

### days_since_last_strategic_bombing
* description: Checks the days since last strategic bombing.
days_since_last_strategic_bombing < 10


### resistance
* description: Compares the current resistance level of a state to a value. Example: resistance > 50 

### resistance_speed
* description: Compares the current resistance speed of a state to a value. Example: resistance_speed > 50 

### resistance_target
* description: Compares the target resistance level of a state to a value. Example: resistance_target > 50 

### state_and_terrain_strategic_value
* description: Checks for state strategic value

### state_population
* description: check the population in the state

### state_population_k
* description: check the population in the state in thousands (use to avoid variable overflows)

### state_strategic_value
* description: Checks for state strategic value

## Dynamic variables for scope unit_leader

### army_attack_level
* description: (Trigger) attack level of the leader

### army_defense_level
* description: (Trigger) defense level of the leader

### attack_level
* description: (Trigger) attack level of the leader

### average_stats
* description: (Trigger) average stats of unit leader

### avg_combat_status
* description: (Trigger) average progress of all combats

### avg_defensive_combat_status
* description: (Trigger) average progress of defensive combats

### avg_offensive_combat_status
* description: (Trigger) average progress of offensive combats

### avg_unit_planning_ratio
* description: (Trigger) average planning ratio of all units

### avg_units_acclimation
* description: (Trigger) average unit acclimatization for a specific climate, acclimatization type is defined in target. example avg_units_acclimation@cold_climate

### coordination_level
* description: (Trigger) coordination level of the leader

### defense_level
* description: (Trigger) defense level of the leader

### has_orders_group
* description: (Trigger) 1 if leader has orders group, zero otherwise

### intel_yield_factor_on_capture
* description: (Trigger) Rate at which intel is extracted from this operative by an enemy country.

### leader_modifier
* description: (Trigger) value of a modifier stored in leader modifier, modifier token is defined in target. example leader_modifier@navy_max_range

### logistics_level
* description: (Trigger) logistics level of the leader

### maneuvering_level
* description: (Trigger) maneuvering level of the leader

### num_armored
* description: (Trigger) number of units with armored dominant type

### num_artillery
* description: (Trigger) number of units with artillery dominant type

### num_assigned_traits
* description: (Trigger) number of assigned traits the leader has

### num_basic_traits
* description: (Trigger) number of basic traits a leader has

### num_battalions
* description: (Trigger) number of battalions

### num_battalions_with_type
* description: (Trigger) number of battalions with sub unit type, sub unit type is defined in target. example: num_battalions_with_type@light_armor

### num_battle_plans
* description: (Trigger) number of battle plans of unit leader

### num_cavalry
* description: (Trigger) number of units with cavalry dominant type

### num_equipment
* description: (Trigger) number of equipment in army of a leader, equipment type token is defined in target. example num_equipment@infantry_equipment

### num_infantry
* description: (Trigger) number of units with infantry dominant type

### num_max_traits
* description: (Trigger) number of maximum assignable traits a leader can have

### num_mechanized
* description: (Trigger) number of units with mechanized dominant type

### num_motorized
* description: (Trigger) number of units with motorized dominant type

### num_personality_traits
* description: (Trigger) number of personality traits a leader has

### num_rocket
* description: (Trigger) number of units with rocket dominant type

### num_ships
* description: (Trigger) number of ships controlled by leader

### num_ships_with_type
* description: (Trigger) number of ships controlled by leader, ship type is defined in target. example num_ships_with_type@carrier

### num_special
* description: (Trigger) number of units with special dominant type

### num_status_traits
* description: (Trigger) number of status traits a leader has

### num_target_equipment
* description: (Trigger) number of equipment required in army of a leader, equipment type token is defined in target. example num_target_equipment@infantry_equipment

### num_terrain_traits
* description: (Trigger) number of terrain traits a leader has

### num_traits
* description: (Trigger) number of traits a leader has

### num_units
* description: (Trigger) number of units controlled by leader

### num_units_crossing_river
* description: (Trigger) number of units currently passing through a river

### num_units_defensive_combats
* description: (Trigger) number of units in defensive combats

### num_units_defensive_combats_on
* description: (Trigger) number of units that are defensively fighting on a terrain, terrain type is defined as target. example: num_units_defensive_combats_on@plains

### num_units_in_combat
* description: (Trigger) number of units current fighting

### num_units_in_state
* description: (Trigger) number of units controlled by leader in state, state is in target. example num_units_in_state@123

### num_units_offensive_combats
* description: (Trigger) number of units in offensive combats

### num_units_offensive_combats_against
* description: (Trigger) number of units that are offensively fighting against a terrain, terrain type is defined as target. example: num_units_offensive_combats_against@plains

### num_units_on_climate
* description: (Trigger) number of units that are on an acclimatization required location, acclimatization type is defined in target. example num_units_on_climate@hot_climate

### num_units_with_type
* description: (Trigger) number of units with dominant type controlled by leader, dominant type is defined in target. example: num_units_with_type@light_armor

### operation_country
* description: (Trigger) the country location the operative is assigned. 0 if it is not assigned to a country

### operation_state
* description: (Trigger) the state location the operative is assigned. 0 if it is not assigned to a state

### operation_type
* description: (Trigger) returns the operation token the operative is assigned

### operative_captor
* description: (Trigger) returns the country tag that captured the operative

### own_capture_chance_factor
* description: (Trigger) The chance this operative has to be captured, taking into account the country it is operating for and the country it is operating against.

### own_forced_into_hiding_time_factor
* description: (Trigger) The time factor applied to the status "forced into hiding". Takes into account the country it is operating for and the country it is operating against.

### own_harmed_time_factor
* description: (Trigger) The time factor applied to the status "harmed". Takes int accountthe country it is operating for and the country it is operating against.

### planning_level
* description: (Trigger) planning level of the leader

### skill_level
* description: (Trigger) skill level of the leader

### sum_unit_terrain_modifier
* description: (Trigger) sum of terrain modifiers of each army's location, terrain type is defined in target. example: sum_unit_terrain_modifier@sickness_chance 

### unit_modifier
* description: (Trigger) value of a modifier stored in unit modifier, modifier token is defined in target. example unit_modifier@army_attack_factor

### unit_ratio_ready_for_plan
* description: (Trigger) ratio of units that are ready for plan

### attack_skill_level
* description: Compares attack skill level of a unit leader.
Example: attack_skill_level > 5

### defense_skill_level
* description: Compares defense skill level of a unit leader.
Example: defense_skill_level > 5

### logistics_skill_level
* description: Compares logistics skill level of a unit leader.
Example: logistics_skill_level > 5

### planning_skill_level
* description: Compares planning skill level of a unit leader.
Example: planning_skill_level > 5

### skill
* description: compare leader skill levels

